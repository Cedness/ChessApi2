package de.ced.chess_api_2;

import de.ced.chess_api_2.game.Position;
import de.ced.chess_api_2.game.Game;

public class Preconditions {

	public static void checkNonNull(Object obj, String name) {
		if (obj == null) {
			throw new NullPointerException(name + " must not be null.");
		}
	}

	public static void checkSizeCompatibility(Game game, Position position) throws IncompatibleSizeException {
		int size = game.getSize();
		if (position.getX() >= size || position.getY() >= size) {
			throw new IncompatibleSizeException(game, position);
		}
	}
}
