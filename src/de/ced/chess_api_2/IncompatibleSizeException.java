package de.ced.chess_api_2;

import de.ced.chess_api_2.game.Position;
import de.ced.chess_api_2.game.Game;

public class IncompatibleSizeException extends Exception {

	private static final long serialVersionUID = 1L;
	private final Game game;
	private final Position position;

	public IncompatibleSizeException(Game game, Position position) {
		this.game = game;
		this.position = position;
	}

	@Override
	public String getMessage() {
		return "Position " + position + " is incompatible for game with size " + game.getSize();
	}
}
