package de.ced.chess_api_2.game;

import de.ced.chess_api_2.game.piecetype.PieceTypeEnum;

public class PlacementInfo {

	private final Position position;
	private final TeamEnum team;
	private final PieceTypeEnum type;
	private final int moves;

	public PlacementInfo(Position position, TeamEnum team, PieceTypeEnum type, int moves) {
		this.position = position;
		this.team = team;
		this.type = type;
		this.moves = moves;
	}

	public PlacementInfo(Position position, TeamEnum team, PieceTypeEnum type) {
		this(position, team, type, 0);
	}

	public Piece toPiece(Game game) {
		return new Piece(game, position, team, type, moves);
	}
}
