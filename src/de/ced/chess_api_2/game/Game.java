package de.ced.chess_api_2.game;

import static de.ced.chess_api_2.game.TeamEnum.*;
import static de.ced.chess_api_2.game.piecetype.PieceTypeEnum.*;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import de.ced.chess_api_2.game.move.Move;
import de.ced.chess_api_2.game.move.Order;

public class Game {

	private static final PlacementInfo[] DEFAULT_PLACEMENTS = {
			new PlacementInfo(new Position(0, 0), WHITE, ROOK),
			new PlacementInfo(new Position(1, 0), WHITE, KNIGHT),
			new PlacementInfo(new Position(2, 0), WHITE, BISHOP),
			new PlacementInfo(new Position(3, 0), WHITE, KING),
			new PlacementInfo(new Position(4, 0), WHITE, QUEEN),
			new PlacementInfo(new Position(5, 0), WHITE, BISHOP),
			new PlacementInfo(new Position(6, 0), WHITE, KNIGHT),
			new PlacementInfo(new Position(7, 0), WHITE, ROOK),
			new PlacementInfo(new Position(0, 1), WHITE, PAWN),
			new PlacementInfo(new Position(1, 1), WHITE, PAWN),
			new PlacementInfo(new Position(2, 1), WHITE, PAWN),
			new PlacementInfo(new Position(3, 1), WHITE, PAWN),
			new PlacementInfo(new Position(4, 1), WHITE, PAWN),
			new PlacementInfo(new Position(5, 1), WHITE, PAWN),
			new PlacementInfo(new Position(6, 1), WHITE, PAWN),
			new PlacementInfo(new Position(7, 1), WHITE, PAWN),
			new PlacementInfo(new Position(0, 6), BLACK, PAWN),
			new PlacementInfo(new Position(1, 6), BLACK, PAWN),
			new PlacementInfo(new Position(2, 6), BLACK, PAWN),
			new PlacementInfo(new Position(3, 6), BLACK, PAWN),
			new PlacementInfo(new Position(4, 6), BLACK, PAWN),
			new PlacementInfo(new Position(5, 6), BLACK, PAWN),
			new PlacementInfo(new Position(6, 6), BLACK, PAWN),
			new PlacementInfo(new Position(7, 6), BLACK, PAWN),
			new PlacementInfo(new Position(0, 7), BLACK, ROOK),
			new PlacementInfo(new Position(1, 7), BLACK, KNIGHT),
			new PlacementInfo(new Position(2, 7), BLACK, BISHOP),
			new PlacementInfo(new Position(3, 7), BLACK, KING),
			new PlacementInfo(new Position(4, 7), BLACK, QUEEN),
			new PlacementInfo(new Position(5, 7), BLACK, BISHOP),
			new PlacementInfo(new Position(6, 7), BLACK, KNIGHT),
			new PlacementInfo(new Position(7, 7), BLACK, ROOK),
	};

	private final Settings settings;

	private final int size;

	private final OrderTaker orderTaker;
	private final Checker checker;
	private final Performer performer;

	private final List<List<Piece>> pieceTable;
	private final List<Piece> pieces;
	private final LinkedHashMap<TeamEnum, Team> teamMap;

	private final Deque<Move> moves;
	private int modCount;
	private boolean virtualMode;

	public Game(Settings settings) {
		this(settings, DEFAULT_PLACEMENTS);
	}

	public Game(Settings settings, PlacementInfo... placements) {
		this.settings = settings;
		size = settings.getSize();

		orderTaker = new OrderTaker(this);
		checker = new Checker(this);
		performer = new Performer(this);

		pieceTable = new ArrayList<>(size);
		for (int y = 0; y < size; y++) {
			List<Piece> line = new ArrayList<>(size);
			for (int x = 0; x < size; x++) {
				line.add(null);
			}
			pieceTable.add(line);
		}
		pieces = new ArrayList<>(32);
		teamMap = new LinkedHashMap<>(2);
		for (TeamEnum team : TeamEnum.values()) {
			teamMap.put(team, new Team(this, team));
		}
		moves = new ArrayDeque<>();

		for (PlacementInfo placementInfo : placements) {
			pieces.add(placementInfo.toPiece(this));
		}
	}

	public Settings getSettings() {
		return settings;
	}

	public int getSize() {
		return size;
	}

	public boolean isValid(int i) {
		return 0 <= i && i < size;
	}

	public boolean isValid(Position position) {
		return isValid(position.getX()) && isValid(position.getY());
	}

	public int makeValid(int i) {
		return Math.min(Math.max(0, i), size - 1);
	}

	public OrderTaker getOrderTaker() {
		return orderTaker;
	}

	public Checker getChecker() {
		return checker;
	}

	public Performer getPerformer() {
		return performer;
	}

	public List<List<Piece>> getPieceTable() {
		return pieceTable;
	}

	public Piece getPieceAtPosition(Position position) {
		return pieceTable.get(position.getY()).get(position.getX());
	}

	public List<Piece> getPieces() {
		return pieces;
	}

	public Map<TeamEnum, Team> getTeamMap() {
		return teamMap;
	}

	public Team getCurrentTeam() {
		Iterator<Team> teamIterator = teamMap.values().iterator();
		Team currentTeam;
		int i = 0;
		do {
			currentTeam = teamIterator.next();
		} while (i++ < moves.size() % teamMap.size());
		return currentTeam;
	}

	public Deque<Move> getMoves() {
		return moves;
	}

	public int getModCount() {
		return modCount;
	}

	public void changeModCount(boolean doOrUndo) {
		modCount += virtualMode && !doOrUndo ? -1 : 1;
	}

	public boolean isVirtualMode() {
		return virtualMode;
	}

	/**
	 * When virtualMode is enabled, no queries will be saved and no listeners will
	 * be called
	 */
	public void setVirtualMode(boolean virtualMode) {
		this.virtualMode = virtualMode;
	}

	public static void main(String[] args) {
		Game game = new Game(new Settings());
		move(game, 5, 1, 5, 3);
		move(game, 4, 6, 4, 5);
		move(game, 4, 0, 7, 3);
	}

	public static void move(Game game, int x1, int y1, int x2, int y2) {
		OrderTaker taker = game.getOrderTaker();
		Order order = new Order(new Position(x1, y1), new Position(x2, y2));
		System.out.println(game.getPieceAtPosition(order.getFrom()).getType().getType());
		System.out.println(taker.analyze(order));
		Move move = taker.getPossibleMove(order);
		System.out.println(move);
		System.out.println("Performed: " + taker.perform(move));
		System.out.println("Checked: " + game.getCurrentTeam().getCheckingPieces().getResult().has());
		System.out.println("Checkmate: " + !game.getCurrentTeam().getPossibleMoves().getResult().has());
		System.out.println(game.getMoves());
		System.out.println("--");
	}
}
