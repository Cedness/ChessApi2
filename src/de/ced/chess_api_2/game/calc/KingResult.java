package de.ced.chess_api_2.game.calc;

import java.util.List;
import java.util.Map;

import de.ced.chess_api_2.game.piecetype.King;

public class KingResult<E> extends ListResult<E> {

	protected Map<King, List<E>> map;

	public KingResult(Map<King, List<E>> map, List<E> all) {
		super(all);
		this.map = map;
	}

	public Map<King, List<E>> getMap() {
		return map;
	}
}
