package de.ced.chess_api_2.game.calc;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import de.ced.chess_api_2.game.Piece;
import de.ced.chess_api_2.game.Team;
import de.ced.chess_api_2.game.TeamEnum;
import de.ced.chess_api_2.game.move.Move;
import de.ced.chess_api_2.game.piecetype.King;

public class CheckingPiecesCalcer extends KingCalcer<Piece> {

	public CheckingPiecesCalcer(Team team) {
		super(team);
	}

	@Override
	protected void kingCalc(Map<King, List<Piece>> map) {
		for (King king : team.getKings()) {
			List<Piece> checkingPieces = new ArrayList<>();
			Piece piece = king.getPiece();
			for (Entry<TeamEnum, Team> entry : game.getTeamMap().entrySet()) {
				if (entry.getValue() == team) {
					continue;
				}
				for (Piece hittingPiece : entry.getValue().getPieces()) {
					for (Move move : hittingPiece.getType().getPossibleMoves()) {
						if (move.getTo().positionEquals(piece.getPosition())) {
							checkingPieces.add(hittingPiece);
						}
					}
				}
			}
			if (checkingPieces.size() > 0) {
				map.put(king, checkingPieces);
			}
		}
	}
}
