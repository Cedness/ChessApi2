package de.ced.chess_api_2.game.calc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.ced.chess_api_2.game.Team;
import de.ced.chess_api_2.game.piecetype.King;

public abstract class KingCalcer<E> extends Calcer<KingResult<E>> {

	public KingCalcer(Team team) {
		super(team);
	}

	@Override
	protected KingResult<E> calc() {
		Map<King, List<E>> map = new HashMap<>();
		kingCalc(map);
		KingResult<E> result = new KingResult<>(map, switch (map.size()) {
		case 0: {
			yield new ArrayList<E>();
		}
		case 1: {
			yield map.values().iterator().next();
		}
		default:
			List<E> all = new ArrayList<>();
			for (List<E> some : map.values()) {
				all.addAll(some);
			}
			yield all;
		});
		return result;
	}

	protected abstract void kingCalc(Map<King, List<E>> map);
}
