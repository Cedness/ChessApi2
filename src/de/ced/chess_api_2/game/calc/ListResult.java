package de.ced.chess_api_2.game.calc;

import java.util.List;

public class ListResult<E> implements Result {

	protected List<E> all;

	public ListResult(List<E> all) {
		this.all = all;
	}

	public List<E> getAll() {
		return all;
	}

	public boolean has() {
		return !all.isEmpty();
	}
}
