package de.ced.chess_api_2.game.calc;

import java.util.List;

import de.ced.chess_api_2.game.Piece;
import de.ced.chess_api_2.game.Team;
import de.ced.chess_api_2.game.move.Move;

public class PossibleMovesCalcer extends ListCalcer<Move> {

	public PossibleMovesCalcer(Team team) {
		super(team);
	}

	@Override
	protected void listCalc(List<Move> all) {
		for (Piece piece : team.getPieces()) {
			for (Move move : piece.getType().getPossibleMoves()) {
				if (game.getChecker().analyzeMove(move).isSuccessful()) {
					all.add(move);
				}
			}
		}
	}
}
