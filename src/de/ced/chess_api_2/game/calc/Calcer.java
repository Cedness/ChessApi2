package de.ced.chess_api_2.game.calc;

import de.ced.chess_api_2.game.Game;
import de.ced.chess_api_2.game.Team;

public abstract class Calcer<R extends Result> {

	protected final Team team;
	protected final Game game;
	protected R result;
	protected int modStamp = -1;

	public Calcer(Team team) {
		this.team = team;
		game = team.getGame();
	}

	public R getResult() {
		if (modStamp == game.getModCount()) {
			return result;
		}
		R result = calc();
		if (!game.isVirtualMode()) {
			modStamp = game.getModCount();
			this.result = result;
		}
		return result;
	}

	protected abstract R calc();
}
