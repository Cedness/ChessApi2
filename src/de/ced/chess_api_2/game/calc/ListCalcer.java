package de.ced.chess_api_2.game.calc;

import java.util.ArrayList;
import java.util.List;

import de.ced.chess_api_2.game.Team;

public abstract class ListCalcer<E> extends Calcer<ListResult<E>> {

	public ListCalcer(Team team) {
		super(team);
	}

	@Override
	protected ListResult<E> calc() {
		List<E> all = new ArrayList<>();
		listCalc(all);
		return new ListResult<>(all);
	}

	protected abstract void listCalc(List<E> all);
}
