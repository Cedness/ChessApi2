package de.ced.chess_api_2.game;

import java.util.ArrayList;
import java.util.List;

import de.ced.chess_api_2.game.calc.CheckingPiecesCalcer;
import de.ced.chess_api_2.game.calc.KingCalcer;
import de.ced.chess_api_2.game.calc.ListCalcer;
import de.ced.chess_api_2.game.calc.PossibleMovesCalcer;
import de.ced.chess_api_2.game.move.Move;
import de.ced.chess_api_2.game.piecetype.King;

public class Team {

	private final Game game;
	private final TeamEnum team;

	private final List<Piece> pieces;
	private final List<Piece> lostPieces;
	private final List<King> kings;

	private final KingCalcer<Piece> checkingPieces;
	private final ListCalcer<Move> possibleMoves;

	public Team(Game game, TeamEnum team) {
		this.game = game;
		this.team = team;
		pieces = new ArrayList<>(16);
		lostPieces = new ArrayList<>(16);
		kings = new ArrayList<>(1);

		checkingPieces = new CheckingPiecesCalcer(this);
		possibleMoves = new PossibleMovesCalcer(this);
	}

	public Game getGame() {
		return game;
	}

	public TeamEnum getType() {
		return team;
	}

	public List<Piece> getPieces() {
		return pieces;
	}

	public List<Piece> getLostPieces() {
		return lostPieces;
	}

	public List<King> getKings() {
		return kings;
	}

	public KingCalcer<Piece> getCheckingPieces() {
		return checkingPieces;
	}

	public ListCalcer<Move> getPossibleMoves() {
		return possibleMoves;
	}
}
