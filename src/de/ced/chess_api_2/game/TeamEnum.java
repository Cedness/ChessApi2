package de.ced.chess_api_2.game;

public enum TeamEnum {

	WHITE("White"),
	BLACK("Black");

	private final String name;

	TeamEnum(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
}
