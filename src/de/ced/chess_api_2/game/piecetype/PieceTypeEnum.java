package de.ced.chess_api_2.game.piecetype;

import de.ced.chess_api_2.game.Piece;

public enum PieceTypeEnum {

	PAWN("Pawn"),
	ROOK("Rook"),
	BISHOP("Bishop"),
	KNIGHT("Knight"),
	QUEEN("Queen"),
	KING("King");

	public static AbstractPieceType createFromEnum(Piece piece, PieceTypeEnum pieceType) {
		return switch (pieceType) {
		case PAWN -> new Pawn(piece);
		case ROOK -> new Rook(piece);
		case BISHOP -> new Bishop(piece);
		case KNIGHT -> new Knight(piece);
		case QUEEN -> new Queen(piece);
		case KING -> new King(piece);
		default -> throw new IllegalArgumentException("Unexpected value: " + pieceType);
		};
	}

	private final String name;

	PieceTypeEnum(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
}