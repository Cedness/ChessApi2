package de.ced.chess_api_2.game.piecetype;

import java.util.List;

import de.ced.chess_api_2.game.Piece;
import de.ced.chess_api_2.game.move.Move;

public class Bishop extends LinePieceType {

	public Bishop(Piece piece) {
		super(piece);
	}

	@Override
	protected void calcMoves(List<Move> targets) {
		calcMovesInDiagonalLines(targets);
	}

	@Override
	public PieceTypeEnum getType() {
		return PieceTypeEnum.BISHOP;
	}
}
