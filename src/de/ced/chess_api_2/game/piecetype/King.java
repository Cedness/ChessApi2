package de.ced.chess_api_2.game.piecetype;

import java.util.List;

import de.ced.chess_api_2.game.Piece;
import de.ced.chess_api_2.game.Position;
import de.ced.chess_api_2.game.move.Move;
import de.ced.chess_api_2.game.move.MoveType;

public class King extends AbstractPieceType {

	public King(Piece piece) {
		super(piece);
		piece.getTeam().getKings().add(this);
	}

	@Override
	protected void calcMoves(List<Move> targets) {
		Position position = piece.getPosition();
		for (int y = -1; y <= 1; y++) {
			for (int x = -1; x <= 1; x++) {
				if (x == 0 && y == 0) {
					continue;
				}
				Position target = position.add(x, y);
				if (!game.isValid(target)) {
					continue;
				}
				Piece hitPiece = game.getPieceAtPosition(target);
				if (hitPiece != null && hitPiece.getTeam() == piece.getTeam()) {
					continue;
				}
				targets.add(new Move(position, target, piece, MoveType.DEFAULT, hitPiece != null, Move.encapsulateCapture(target, hitPiece)));
			}
		}
		// Castling
		if (game.getSettings().isCastling()) {

		}
	}

	@Override
	public void remove() {
		super.remove();
		piece.getTeam().getKings().remove(this);
	}

	@Override
	public PieceTypeEnum getType() {
		return PieceTypeEnum.KING;
	}
}
