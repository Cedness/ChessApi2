package de.ced.chess_api_2.game.piecetype;

import java.util.ArrayList;
import java.util.List;

import de.ced.chess_api_2.game.Piece;
import de.ced.chess_api_2.game.Position;
import de.ced.chess_api_2.game.move.Move;
import de.ced.chess_api_2.game.move.MoveType;

public class Knight extends AbstractPieceType {

	private static final List<Position> relativeTargets = new ArrayList<>();

	static {
		for (int y = -2; y <= 2; y++) {
			for (int x = -2; x <= 2; x++) {
				if (y == 0 || x == 0 || Math.abs(y) == Math.abs(x)) {
					continue;
				}
				relativeTargets.add(new Position(x, y));
			}
		}
	}

	public Knight(Piece piece) {
		super(piece);
	}

	@Override
	protected void calcMoves(List<Move> targets) {
		Position position = piece.getPosition();
		for (Position relative : relativeTargets) {
			Position target = position.add(relative);
			if (!game.isValid(target)) {
				continue;
			}
			Piece capturePiece = game.getPieceAtPosition(target);
			boolean captures = capturePiece != null;
			if (captures && capturePiece.getTeam() == piece.getTeam()) {
				continue;
			}
			targets.add(new Move(position, target, piece, MoveType.DEFAULT, captures, Move.encapsulateCapture(target, capturePiece)));
		}
	}

	@Override
	public PieceTypeEnum getType() {
		return PieceTypeEnum.KNIGHT;
	}
}
