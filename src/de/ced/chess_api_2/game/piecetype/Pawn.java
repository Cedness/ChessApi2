package de.ced.chess_api_2.game.piecetype;

import java.util.Iterator;
import java.util.List;

import de.ced.chess_api_2.game.Piece;
import de.ced.chess_api_2.game.Position;
import de.ced.chess_api_2.game.TeamEnum;
import de.ced.chess_api_2.game.move.Move;
import de.ced.chess_api_2.game.move.MoveType;

public class Pawn extends AbstractPieceType {

	private final int direction;

	public Pawn(Piece piece) {
		super(piece);
		direction = piece.getTeam().getType() == TeamEnum.WHITE ? 1 : -1;
	}

	@Override
	protected void calcMoves(List<Move> targets) {
		Position position = piece.getPosition();
		Position target = position.add(0, direction);
		// One step
		if (game.isValid(target)) {
			if (game.getPieceAtPosition(target) == null) {
				targets.add(new Move(position, target, piece, determineMoveType(target), false));
				// Two steps
				if (piece.getMoves() == 0) {
					Position farTarget = target.add(0, 1);
					if (game.isValid(farTarget) && game.getPieceAtPosition(farTarget) == null) {
						targets.add(new Move(position, farTarget, piece, determineMoveType(target), false));
					}
				}
			}
			// Diagonal hit
			for (int x = -1; x <= 1; x += 2) {
				Position hitTarget = target.add(x, 0);
				if (!game.isValid(hitTarget)) {
					continue;
				}
				Piece hitPiece = game.getPieceAtPosition(hitTarget);
				if (hitPiece == null || hitPiece.getTeam() == piece.getTeam()) {
					continue;
				}
				targets.add(new Move(position, hitTarget, piece, determineMoveType(target), true, Move.encapsulateCapture(hitTarget, hitPiece)));
			}
			// En passant
			if (game.getSettings().isEnPassant()) {
				for (int x = -1; x <= 1; x += 2) {
					Position walkTarget = target.add(x, 0);
					Position hitTarget = position.add(x, 0);
					if (!game.isValid(walkTarget) || !game.isValid(hitTarget)) {
						continue;
					}
					Piece hitPiece = game.getPieceAtPosition(hitTarget);
					if (hitPiece == null || hitPiece.getTeam() == piece.getTeam() || !(hitPiece.getType() instanceof Pawn) || hitPiece.getMoves() != 1) {
						continue;
					}
					Iterator<Move> iterator = game.getMoves().iterator();
					for (int i = 0; iterator.hasNext() && i < game.getTeamMap().size() - 1; i++) {
						Move move = iterator.next();
						if (move.getPiece() != hitPiece || Math.abs(move.getTo().getY() - move.getFrom().getX()) != 2) {
							continue;
						}
						targets.add(new Move(position, walkTarget, piece, determineMoveType(target), true, Move.encapsulateCapture(hitTarget, hitPiece)));
						break;
					}
				}
			}
		}
	}

	private MoveType determineMoveType(Position target) {
		return target.getY() == (direction < 0 ? 0 : game.getSize() - 1) ? MoveType.PROMOTION : MoveType.DEFAULT;
	}

	@Override
	public PieceTypeEnum getType() {
		return PieceTypeEnum.PAWN;
	}
}
