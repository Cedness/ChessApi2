package de.ced.chess_api_2.game.piecetype;

import java.util.List;

import de.ced.chess_api_2.game.Piece;
import de.ced.chess_api_2.game.move.Move;

public class Rook extends LinePieceType {

	public Rook(Piece piece) {
		super(piece);
	}

	@Override
	protected void calcMoves(List<Move> targets) {
		calcMovesInStraightLines(targets);
	}

	@Override
	public PieceTypeEnum getType() {
		return PieceTypeEnum.ROOK;
	}
}
