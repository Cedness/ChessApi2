package de.ced.chess_api_2.game.piecetype;

import java.util.ArrayList;
import java.util.List;

import de.ced.chess_api_2.game.Game;
import de.ced.chess_api_2.game.Piece;
import de.ced.chess_api_2.game.move.Move;

public abstract class AbstractPieceType {

	protected final Piece piece;
	protected final Game game;

	protected List<Move> possibleMoves;
	protected int modStamp = -1;

	public AbstractPieceType(Piece piece) {
		this.piece = piece;
		game = piece.getGame();
	}

	public Piece getPiece() {
		return piece;
	}

	public boolean checkTypeEquality(AbstractPieceType pieceType) {
		return pieceType != null && getClass().equals(pieceType.getClass());
	}

	public List<Move> getPossibleMoves() {
		int currentModStamp = game.getModCount();
		if (modStamp == currentModStamp) {
			return possibleMoves;
		}
		List<Move> possibleMoves = new ArrayList<>();
		calcMoves(possibleMoves);
		if (!game.isVirtualMode()) {
			modStamp = currentModStamp;
			this.possibleMoves = possibleMoves;
		}
		return possibleMoves;
	}

	protected abstract void calcMoves(List<Move> targets);

	public void remove() {
	}

	public abstract PieceTypeEnum getType();

	@Override
	public String toString() {
		return getType().getName();
	}
}
