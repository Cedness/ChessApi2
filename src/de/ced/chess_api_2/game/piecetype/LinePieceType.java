package de.ced.chess_api_2.game.piecetype;

import java.util.List;

import de.ced.chess_api_2.game.Piece;
import de.ced.chess_api_2.game.Position;
import de.ced.chess_api_2.game.move.Move;
import de.ced.chess_api_2.game.move.MoveType;

public abstract class LinePieceType extends AbstractPieceType {

	public LinePieceType(Piece piece) {
		super(piece);
	}

	protected void calcMovesInStraightLines(List<Move> targets) {
		for (int i = -1; i <= 1; i += 2) {
			calcMovesInLine(targets, new Position(i, 0));
			calcMovesInLine(targets, new Position(0, i));
		}
	}

	protected void calcMovesInDiagonalLines(List<Move> targets) {
		for (int y = -1; y <= 1; y += 2) {
			for (int x = -1; x <= 1; x += 2) {
				calcMovesInLine(targets, new Position(x, y));
			}
		}
	}

	private void calcMovesInLine(List<Move> targets, Position step) {
		Position position = piece.getPosition();
		Position target = position;
		while (true) {
			target = target.add(step);
			boolean captures;
			Piece capturePiece;
			if (!game.isValid(target) || (captures = (capturePiece = game.getPieceAtPosition(target)) != null) && capturePiece.getTeam() == piece.getTeam()) {
				return;
			}
			targets.add(new Move(position, target, piece, MoveType.DEFAULT, captures, Move.encapsulateCapture(target, capturePiece)));
			if (captures) {
				return;
			}
		}
	}
}
