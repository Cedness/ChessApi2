package de.ced.chess_api_2.game;

public class Position {

	public static final Position INVALID = new Position(-1, -1);

	// 30, 65, 97

	public static char toNumber(int x) {
		return (char) (x + '1');
	}

	public static char toLower(int x) {
		return (char) (x + 'a');
	}

	public static char toUpper(int x) {
		return (char) (x + 'A');
	}

	public static int fromChar(char c) {
		return c - (c >= 'A' ? 'A' : 'a');
	}

	private final int x;
	private final int y;

	public Position(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public boolean isValid() {
		return x >= 0;
	}

	public boolean positionEquals(Position position) {
		return position != null && x == position.x && y == position.y;
	}

	@Override
	public String toString() {
		return toLower(x) + "" + toNumber(y);
	}

	public Position add(Position relative) {
		return add(relative.x, relative.y);
	}

	public Position add(int x, int y) {
		return new Position(this.x + x, this.y + y);
	}
}
