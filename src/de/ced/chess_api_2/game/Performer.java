package de.ced.chess_api_2.game;

import de.ced.chess_api_2.game.move.Move;
import de.ced.chess_api_2.game.move.MoveType;
import de.ced.chess_api_2.game.move.PieceMove;
import de.ced.chess_api_2.game.move.Promotion;
import de.ced.chess_api_2.game.piecetype.PieceTypeEnum;

public class Performer {

	private final Game game;

	public Performer(Game game) {
		this.game = game;
	}

	public boolean applyMove(Move move, boolean allowPromotionWithoutPromotion) {
		if (!allowPromotionWithoutPromotion && move.getType() == MoveType.PROMOTION && !(move instanceof Promotion)) {
			return false;
		}
		game.getMoves().push(move);
		for (PieceMove secondaryMove : move.getSecundaryMoves()) {
			perform(secondaryMove, true);
		}
		perform(move, true);
		if (move instanceof Promotion promotion) {
			promotion.getPiece().setPieceType(promotion.getPromotionType());
		}
		game.changeModCount(true);
		return true;
	}

	public Move undo() {
		Move move = game.getMoves().poll();
		if (move != null) {
			if (move instanceof Promotion promotion) {
				promotion.getPiece().setPieceType(PieceTypeEnum.PAWN);
			}
			perform(move, false);
			for (PieceMove secondaryMove : move.getSecundaryMoves()) {
				perform(secondaryMove, false);
			}
			game.changeModCount(false);
		}
		return move;
	}

	private void perform(PieceMove pieceMove, boolean forward) {
		Piece piece = pieceMove.getPiece();
		piece.setPosition(forward ? pieceMove.getTo() : pieceMove.getFrom(), forward);
	}
}
