package de.ced.chess_api_2.game;

public class Settings {

	private int size = 8;
	private boolean castling = true;
	private boolean enPassant = true;
	private boolean promotion = true;
	private int drawOnRepetition = 5;
	private int drawOnConsecutiveRepetition = 0;
	private int drawAfterMovesWithoutPawnMoveOrCapture = 75;
	private boolean drawOnDeadPosition = true;
	private boolean checkmateIfOneKingDown = true;

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public boolean isCastling() {
		return castling;
	}

	public void setCastling(boolean castling) {
		this.castling = castling;
	}

	public boolean isEnPassant() {
		return enPassant;
	}

	public void setEnPassant(boolean enPassant) {
		this.enPassant = enPassant;
	}

	public boolean isPromotion() {
		return promotion;
	}

	public void setPromotion(boolean promotion) {
		this.promotion = promotion;
	}

	public int getDrawOnRepetition() {
		return drawOnRepetition;
	}

	public void setDrawOnRepetition(int drawOnRepetition) {
		this.drawOnRepetition = drawOnRepetition;
	}

	public int getDrawOnConsecutiveRepetition() {
		return drawOnConsecutiveRepetition;
	}

	public void setDrawOnConsecutiveRepetition(int drawOnConsecutiveRepetition) {
		this.drawOnConsecutiveRepetition = drawOnConsecutiveRepetition;
	}

	public int getDrawAfterMovesWithoutPawnMoveOrCapture() {
		return drawAfterMovesWithoutPawnMoveOrCapture;
	}

	public void setDrawAfterMovesWithoutPawnMoveOrCapture(int drawAfterMovesWithoutPawnMoveOrCapture) {
		this.drawAfterMovesWithoutPawnMoveOrCapture = drawAfterMovesWithoutPawnMoveOrCapture;
	}

	public boolean isDrawOnDeadPosition() {
		return drawOnDeadPosition;
	}

	public void setDrawOnDeadPosition(boolean drawOnDeadPosition) {
		this.drawOnDeadPosition = drawOnDeadPosition;
	}

	public boolean isCheckmateIfOneKingDown() {
		return checkmateIfOneKingDown;
	}

	public void setCheckmateIfOneKingDown(boolean checkmateIfOneKingDown) {
		this.checkmateIfOneKingDown = checkmateIfOneKingDown;
	}
}
