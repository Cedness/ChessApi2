package de.ced.chess_api_2.game;

import de.ced.chess_api_2.Preconditions;
import de.ced.chess_api_2.game.move.Move;
import de.ced.chess_api_2.game.move.Order;

public class OrderTaker {

	private final Game game;
	private int modStamp = -1;

	private Position start;
	private AnalysisResult startAnalysisResult;
	private Order order;
	private Position target;
	private Move move;
	private AnalysisResult orderAnalysisResult;

	public OrderTaker(Game game) {
		this.game = game;
	}

	void setMove(Move move) {
		this.move = move;
	}

	private boolean checkModStamp() {
		if (modStamp != game.getModCount()) {
			modStamp = game.getModCount();
			start = null;
			startAnalysisResult = null;
			order = null;
			target = null;
			move = null;
			orderAnalysisResult = null;
			return false;
		}
		return true;
	}

	public AnalysisResult analyze(Position start) {
		Preconditions.checkNonNull(start, "Start-Position");
		if (checkModStamp() && start.positionEquals(this.start)) {
			return startAnalysisResult;
		}
		this.start = start;
		return startAnalysisResult = game.getChecker().analyzeStart(start);
	}

	public AnalysisResult analyze(Order order) {
		Preconditions.checkNonNull(order, "Order");
		if (checkModStamp() && order.contentEquals(this.order)) {
			return orderAnalysisResult;
		}
		this.order = order;
		target = order.getTo();
		return orderAnalysisResult = analyze(order.getFrom()).isSuccessful() ? game.getChecker().analyzeTarget(start, target, true) : startAnalysisResult;
	}

	public Move getPossibleMove(Order order) {
		return analyze(order).isSuccessful() ? move : null;
	}

	public boolean perform(Move move) {
		if (!checkModStamp() || this.move == null || !this.move.contentEquals(move)) {
			return false;
		}
		return game.getPerformer().applyMove(move, false);
	}

	public Move undo() {
		return game.getPerformer().undo();
	}
}
