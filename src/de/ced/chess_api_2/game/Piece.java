package de.ced.chess_api_2.game;

import java.util.List;

import de.ced.chess_api_2.Preconditions;
import de.ced.chess_api_2.game.piecetype.AbstractPieceType;
import de.ced.chess_api_2.game.piecetype.PieceTypeEnum;

public class Piece {

	private final Game game;
	private Position position;
	private Team team;
	private AbstractPieceType pieceType;
	private int moves;

	public Piece(Game game, Position position, TeamEnum team, PieceTypeEnum pieceType, int moves) {
		Preconditions.checkNonNull(game, "Game");
		this.game = game;
		setPosition(position, true);
		setTeam(team);
		setPieceType(pieceType); // Depends on Team
		this.moves = moves;
	}

	public Game getGame() {
		return game;
	}

	public void remove() {
		setPosition(Position.INVALID, true);
	}

	public Position getPosition() {
		return position;
	}

	public void setPosition(Position position, boolean doOrUndo) {
		Preconditions.checkNonNull(position, "Position");
		if (position.positionEquals(this.position)) {
			return;
		}
		moves += doOrUndo ? 1 : -1;

		List<List<Piece>> pieceTable = game.getPieceTable();
		if (this.position != null && this.position.isValid()) {

			pieceTable.get(this.position.getY()).set(this.position.getX(), null);
		}
		if (position.isValid()) {
			pieceTable.get(position.getY()).set(position.getX(), this);
		}

		this.position = position;
	}

	public Team getTeam() {
		return team;
	}

	private void setTeam(TeamEnum teamEnum) {
		Preconditions.checkNonNull(teamEnum, "Team");

		if (team != null) {
			if (teamEnum.equals(team.getType())) {
				return;
			}
			team.getPieces().remove(this);
		}
		Team team = game.getTeamMap().get(teamEnum);
		team.getPieces().add(this);

		this.team = team;
	}

	public AbstractPieceType getType() {
		return pieceType;
	}

	public void setPieceType(PieceTypeEnum pieceType) {
		Preconditions.checkNonNull(pieceType, "PieceType");
		if (this.pieceType != null && pieceType.equals(this.pieceType.getType())) {
			return;
		}

		if (this.pieceType != null) {
			this.pieceType.remove();
		}

		this.pieceType = PieceTypeEnum.createFromEnum(this, pieceType);
	}

	public int getMoves() {
		return moves;
	}
}
