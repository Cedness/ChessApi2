package de.ced.chess_api_2.game.move;

public enum MoveType {

	DEFAULT(),
	CASTLING(),
	EN_PASSANT(),
	PROMOTION(),
	;
}
