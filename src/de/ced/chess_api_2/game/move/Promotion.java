package de.ced.chess_api_2.game.move;

import de.ced.chess_api_2.Preconditions;
import de.ced.chess_api_2.game.Piece;
import de.ced.chess_api_2.game.Position;
import de.ced.chess_api_2.game.piecetype.PieceTypeEnum;

public class Promotion extends Move {

	protected final PieceTypeEnum promotionType;

	@Deprecated
	private Promotion(Position from, Position to, Piece piece, MoveType moveType, boolean captures, PieceTypeEnum promotionType, PieceMove... secundaryMoves) {
		super(from, to, piece, moveType, captures, secundaryMoves);
		this.promotionType = promotionType;
	}

	public Promotion(Move move, PieceTypeEnum promotionType) {
		super(move.from, move.to, move.piece, move.moveType, move.captures, move.secundaryMoves.toArray(new PieceMove[move.secundaryMoves.size()]));
		Preconditions.checkNonNull(promotionType, "PieceType");
		if (move.getType() != MoveType.PROMOTION) {
			throw new RuntimeException("A promotion can only be created from a move of type promotion.");
		}
		this.promotionType = promotionType;
	}

	public PieceTypeEnum getPromotionType() {
		return promotionType;
	}

	public boolean contentEquals(Promotion promotion) {
		return contentEquals((Move) promotion) && promotionType == promotion.promotionType;
	}
}
