package de.ced.chess_api_2.game.move;

import de.ced.chess_api_2.game.Piece;
import de.ced.chess_api_2.game.Position;

public class PieceMove extends Order {

	protected final Piece piece;

	public PieceMove(Position from, Position to, Piece piece) {
		super(from, to);
		this.piece = piece;
	}

	public Piece getPiece() {
		return piece;
	}

	public boolean contentEquals(PieceMove pieceMove) {
		return contentEquals((Order) pieceMove) && piece == pieceMove.piece;
	}
}
