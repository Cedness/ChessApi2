package de.ced.chess_api_2.game.move;

import de.ced.chess_api_2.Preconditions;
import de.ced.chess_api_2.game.Position;

public class Order {

	protected final Position from;
	protected final Position to;

	public Order(Position from, Position to) {
		Preconditions.checkNonNull(from, "Start-Position");
		Preconditions.checkNonNull(to, "Target-Position");
		this.from = from;
		this.to = to;
	}

	public Position getFrom() {
		return from;
	}

	public Position getTo() {
		return to;
	}

	public boolean contentEquals(Order order) {
		return order != null && from.positionEquals(order.from) && to.positionEquals(order.to);
	}

	@Override
	public String toString() {
		return from.toString() + " " + to.toString();
	}
}
