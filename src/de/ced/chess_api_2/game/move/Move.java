package de.ced.chess_api_2.game.move;

import java.util.Iterator;
import java.util.List;

import de.ced.chess_api_2.game.Piece;
import de.ced.chess_api_2.game.Position;
import de.ced.chess_api_2.game.piecetype.PieceTypeEnum;

public class Move extends PieceMove {

	protected final MoveType moveType;
	protected final boolean captures;
	protected final List<PieceMove> secundaryMoves;

	public Move(Position from, Position to, Piece piece, MoveType moveType, boolean captures, PieceMove... secundaryMoves) {
		super(from, to, piece);
		this.moveType = moveType;
		this.captures = captures;
		this.secundaryMoves = List.of(secundaryMoves);
	}

	public MoveType getType() {
		return moveType;
	}

	public boolean captures() {
		return captures;
	}

	public List<PieceMove> getSecundaryMoves() {
		return secundaryMoves;
	}

	public boolean contentEquals(Move move) {
		if (!contentEquals((PieceMove) move) || captures != move.captures || secundaryMoves.size() != move.secundaryMoves.size()) {
			return false;
		}
		for (Iterator<PieceMove> it = secundaryMoves.iterator(), otherIt = move.secundaryMoves.iterator(); it.hasNext();) {
			if (!it.next().contentEquals(otherIt.next())) {
				return false;
			}
		}
		return true;
	}

	public Promotion cloneWithPromotion(PieceTypeEnum promotionType) {
		return new Promotion(this, promotionType);
	}

	public static PieceMove[] encapsulateCapture(Position target, Piece capturePiece) {
		return capturePiece != null ? new PieceMove[] { new PieceMove(target, Position.INVALID, capturePiece) } : new PieceMove[0];
	}
}
