package de.ced.chess_api_2.game;

import static de.ced.chess_api_2.game.AnalysisResult.*;

import java.util.List;

import de.ced.chess_api_2.game.move.Move;
import de.ced.chess_api_2.game.move.MoveType;

public class Checker {

	private final Game game;
	private Move move;

	public Checker(Game game) {
		this.game = game;
	}

	public AnalysisResult analyzeStart(Position start) {
		if (!game.isValid(start)) {
			return INVALID_START_POSITION;
		}
		Piece piece = game.getPieceAtPosition(start);
		if (piece == null) {
			return EMPTY_FIELD;
		}
		if (piece.getTeam() != game.getCurrentTeam()) {
			return INVALID_PEACE;
		}
		List<Move> possibleMoves = piece.getType().getPossibleMoves();
		if (possibleMoves.isEmpty()) {
			return NO_TARGETS;
		}
		return DEFAULT;
	}

	public AnalysisResult analyzeTarget(Position start, Position target) {
		return analyzeTarget(start, target, false);
	}

	AnalysisResult analyzeTarget(Position start, Position target, boolean saveMove) {
		if (!game.isValid(target)) {
			return INVALID_TARGET_POSITION;
		}
		if (start.positionEquals(target)) {
			return START_EQUALS_TARGET;
		}
		for (Move move : game.getPieceAtPosition(start).getType().getPossibleMoves()) {
			if (target.positionEquals(move.getTo())) {
				return analyzeMove(move, saveMove);
			}
		}
		return TARGET_NOT_IN_REACH;
	}

	public AnalysisResult analyzeMove(Move move) {
		return analyzeMove(move, false);
	}

	private AnalysisResult analyzeMove(Move move, boolean saveMove) {
		Performer performer = game.getPerformer();
		Team currentTeam = game.getCurrentTeam();
		boolean isChecked = currentTeam.getCheckingPieces().getResult().has();
		game.setVirtualMode(true);
		performer.applyMove(move, true);
		boolean willBeChecked = currentTeam.getCheckingPieces().getResult().has();
		performer.undo();
		game.setVirtualMode(false);
		if (willBeChecked) {
			return isChecked ? STAY_CHECKED : MOVE_TO_CHECK;
		}
		// TODO Castling
		if (saveMove) {
			game.getOrderTaker().setMove(move);
		}
		return move.getType() == MoveType.PROMOTION ? SPECIFY_PROMOTION : DEFAULT;
	}
}
