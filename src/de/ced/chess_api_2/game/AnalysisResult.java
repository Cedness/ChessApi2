package de.ced.chess_api_2.game;

/**
 * Stores information about whether a move has been performed or why it has not
 * been performed.
 */
public enum AnalysisResult {

	/**
	 * NOT performed because the start position is not valid (x or y not between 1
	 * and 8)
	 */
	INVALID_START_POSITION(false, FaultPhase.WHILE_ANALYZING_START),
	/**
	 * NOT performed because the start position does not contain a Piece
	 */
	EMPTY_FIELD(false, FaultPhase.WHILE_ANALYZING_START),
	/**
	 * NOT performed because the selected Piece has the wrong color
	 */
	INVALID_PEACE(false, FaultPhase.WHILE_ANALYZING_START),
	/**
	 * NOT performed because there are no targets available for the selected Piece
	 */
	NO_TARGETS(false, FaultPhase.WHILE_ANALYZING_START),
	/**
	 * NOT performed because the target position is not valid (x or y not between 1
	 * and 8)
	 */
	INVALID_TARGET_POSITION(false, FaultPhase.WHILE_ANALYZING_TARGET),
	/**
	 * NOT performed because the start position equals the target position
	 */
	START_EQUALS_TARGET(false, FaultPhase.WHILE_ANALYZING_TARGET),
	/**
	 * NOT performed because the selected Piece cannot reach the target position
	 * (will be NO_TARGETS if there are no targets at all for this Piece)
	 */
	TARGET_NOT_IN_REACH(false, FaultPhase.WHILE_ANALYZING_TARGET),
	/**
	 * NOT performed because moving the selected Piece to the target position would
	 * cause the moving team to become checked
	 */
	MOVE_TO_CHECK(false, FaultPhase.WHILE_ANALYZING_TARGET),
	/**
	 * NOT performed because moving the selected Piece to the target position would
	 * cause the moving team to stay checked
	 */
	STAY_CHECKED(false, FaultPhase.WHILE_ANALYZING_TARGET),
	/**
	 * NOT performed because Rochade is not allowed while king is checked
	 */
	ROCHADE_WHILE_CHECKED(false, FaultPhase.WHILE_ANALYZING_TARGET),
	/**
	 * NOT performed because Rochade is not allowed if the king has to cross a
	 * checked field
	 */
	ROCHADE_OVER_CHECKED_FIELD(false, FaultPhase.WHILE_ANALYZING_TARGET),
	/**
	 * Performed without fault
	 */
	DEFAULT(true, FaultPhase.DEFAULT),
	/**
	 * Performed without fault, but a promotion-type needs to be specified before
	 * move can start
	 */
	SPECIFY_PROMOTION(true, FaultPhase.DEFAULT);

	private final boolean successful;
	private final FaultPhase faultPhase;

	AnalysisResult(boolean successful, FaultPhase faultPhase) {
		this.successful = successful;
		this.faultPhase = faultPhase;
	}

	public boolean isSuccessful() {
		return successful;
	}

	public FaultPhase getFaultPhase() {
		return faultPhase;
	}

	public enum FaultPhase {
		WHILE_ANALYZING_START,
		WHILE_ANALYZING_TARGET,
		DEFAULT
	}
}
